#include "ofApp.h"

//--------------------------------------------------------------
void ofApp::setup(){

//    ofBackground(10,10,220);
    ofEnableDepthTest();
    ofSetVerticalSync(0);
    ofSetFrameRate(120);
    isPlaying=1;
    win=false;


    
    pieces[0].push_back(new Pawn(ofVec3f(3,0,1), 0) );
    pieces[0].push_back(new Pawn(ofVec3f(2,0,1), 0) );
    pieces[0].push_back(new Pawn(ofVec3f(1,0,1), 0) );
    pieces[0].push_back(new Pawn(ofVec3f(0,0,1), 0) );
    pieces[0].push_back(new Pawn(ofVec3f(3,1,1), 0) );
    pieces[0].push_back(new Pawn(ofVec3f(2,1,1), 0) );
    pieces[0].push_back(new Pawn(ofVec3f(1,1,1), 0) );
    pieces[0].push_back(new Pawn(ofVec3f(0,1,1), 0) );
     
    
    pieces[0].push_back(new Rook(ofVec3f(0,0,0), 0) );
    pieces[0].push_back(new Rook(ofVec3f(3,0,0), 0) );
    
    pieces[0].push_back(new Bishop(ofVec3f(1,0,0), 0) );
    pieces[0].push_back(new Bishop(ofVec3f(2,0,0), 0) );
    
    pieces[0].push_back(new Knight(ofVec3f(0,1,0), 0) );
    pieces[0].push_back(new Knight(ofVec3f(3,1,0), 0) );
    
    pieces[0].push_back(new King(ofVec3f(2,1,0), 0) );
    pieces[0].push_back(new Queen(ofVec3f(1,1,0), 0) );
    
    
    
    pieces[1].push_back(new Pawn(ofVec3f(3,2,2), 1) );
    pieces[1].push_back(new Pawn(ofVec3f(2,2,2), 1) );
    pieces[1].push_back(new Pawn(ofVec3f(1,2,2), 1) );
    pieces[1].push_back(new Pawn(ofVec3f(0,2,2), 1) );
    pieces[1].push_back(new Pawn(ofVec3f(3,3,2), 1) );
    pieces[1].push_back(new Pawn(ofVec3f(2,3,2), 1) );
    pieces[1].push_back(new Pawn(ofVec3f(1,3,2), 1) );
    pieces[1].push_back(new Pawn(ofVec3f(0,3,2), 1) );
     
    
    pieces[1].push_back(new Rook(ofVec3f(0,3,3), 1) );
    pieces[1].push_back(new Rook(ofVec3f(3,3,3), 1) );
    
    pieces[1].push_back(new Bishop(ofVec3f(1,3,3), 1) );
    pieces[1].push_back(new Bishop(ofVec3f(2,3,3), 1) );
    
    pieces[1].push_back(new Knight(ofVec3f(0,2,3), 1) );
    pieces[1].push_back(new Knight(ofVec3f(3,2,3), 1) );
    
    pieces[1].push_back(new King(ofVec3f(2,2,3), 1) );
    pieces[1].push_back(new Queen(ofVec3f(1,2,3), 1) );
     
    pieces[1][8]->positions=&positions;
    pieces[1][9]->positions=&positions;
    pieces[1][10]->positions=&positions;
    pieces[1][11]->positions=&positions;
    pieces[1][15]->positions=&positions;

    
    for(int i=0;i<2;i++)
    {
        for(int a=0;a<pieces[i].size();a++)
        {
            pieces[i][a]->o=pieces[0];
            pieces[i][a]->o.insert(pieces[i][a]->o.end(), pieces[1].begin(), pieces[1].end());
        }
    }

}

//--------------------------------------------------------------
void ofApp::update(){
    
    if(ofGetFrameNum()<=2)
    {
        cam.setPosition(200, 200, 1300);
    }
}

//--------------------------------------------------------------
void ofApp::draw(){
    
    positions.clear();
    ofDrawBitmapString("value: " + ofToString(ofGetFrameRate()), 10, 10);
    ofBackgroundGradient(ofColor(15,15,15 ), ofColor(0,0,255), OF_GRADIENT_LINEAR);

    
     cam.begin();
    
    board.draw();
  
//    board.draw();
    
    ofDisableDepthTest();
    for(int n=0;n<2;n++)
    {
        for(int i=0;i<pieces[n].size();i++)
        {
            pieces[n][i]->o.clear();
            
            pieces[n][i]->o=pieces[0];
            pieces[n][i]->o.insert(pieces[n][i]->o.end(), pieces[1].begin(), pieces[1].end());
            
            pieces[n][i]->draw();
            
            if(win)
            {
                if(winColor)
                 ofDrawBitmapString( "WHITE WINS", 250, 684, 200);
                else
                 ofDrawBitmapString( "BLACK WINS", 250, 684, 200);
            }
        }
    }
    ofEnableDepthTest();
    
    cam.end();
}

//--------------------------------------------------------------
void ofApp::keyPressed(int key){

    
    if (key == OF_KEY_LEFT){
        
        cam.move(-5, 0, 0);
    }
    
    if (key == OF_KEY_RIGHT){
        
        cam.move(5, 0, 0);
    }
    
    if (key == OF_KEY_UP){
        
        cam.move(0, 0, -5);
    }
    
    if (key == OF_KEY_DOWN){
        
        cam.move(0, 0, 5);
    }
    
}

//--------------------------------------------------------------
void ofApp::keyReleased(int key){

}

//--------------------------------------------------------------
void ofApp::mouseMoved(int x, int y ){

}

//--------------------------------------------------------------
void ofApp::mouseDragged(int x, int y, int button){

}

//--------------------------------------------------------------
void ofApp::mousePressed(int x, int y, int button){
    
   
    ofVec3f index;
    int dist=99999;
  
    for(int x=0;x<4;x++)
    {
        for(int y=0;y<4;y++)
        {
            for(int z=0;z<4;z++)
            {
                if( ofDist(cam.worldToScreen(ofVec3f(x*150,y*150,z*150)).x, cam.worldToScreen(ofVec3f(x*150,y*150,z*150)).y, mouseX, mouseY) < dist)
                {
                    dist = ofDist(cam.worldToScreen(ofVec3f(x*150,y*150,z*150)).x, cam.worldToScreen(ofVec3f(x*150,y*150,z*150)).y, mouseX, mouseY);
                    if(dist < 150)
                    {
                        index=ofVec3f(x,y,z);
                    }
                }
            }
        }
    }
    
        if( selected==index)
        {
            for(int n=0;n<2;n++)
            {
                for(int i=0;i<pieces[n].size();i++)
                {
                    if(index==pieces[n][i]->position)
                    {
                        pieces[n][i]->selected=false;//click twice on same piece to unselect it
                    }
                }
            }

            index=ofVec3f(99,99,99);//click twice on same cube to unselect
            
        }
    /*
    for (int n=0; n<2; n++)
    {
        for(int i=0;i<pieces[n].size();i++)
        {
            if(index==pieces[n][i]->position )
            {
                for (int n=0; n<2; n++)
                {
                    for(int i=0;i<pieces[n].size();i++)
                    {
                     if(pieces[n][i]->selected)
                     {
                         pieces[n][i]->selected=false;
                     }
                    }
                }
              //  std::cout<<isPlaying<<"   "<<pieces[n][i]->color<<endl;
                //if(isPlaying == pieces[n][i]->color)
                //pieces[n][i]->selected=true;
            }
        }
    }
    */
    for(int i=0;i<pieces[isPlaying].size();i++)
    {
        if(pieces[isPlaying][i]->selected==true && index != ofVec3f(99,99,99) && index!=pieces[isPlaying][i]->position)
        {
            if(pieces[isPlaying][i]->move(index))
            {
            for(int i=0;i<pieces[!isPlaying].size();i++)
            {
                if( pieces[!isPlaying][i]->alive==0 )
                {
                    if(pieces[!isPlaying][i]->isKing())
                    {
                        win=true;
                        winColor=isPlaying;
                    }
                        
                    delete pieces[!isPlaying][i];
                    pieces[!isPlaying][i] = nullptr;
                }
            }
            
            pieces[!isPlaying].erase(std::remove(pieces[!isPlaying].begin(), pieces[!isPlaying].end(), nullptr), pieces[!isPlaying].end());
            
            pieces[isPlaying][i]->selected=false;
            index = ofVec3f(99,99,99);
            
            isPlaying = !isPlaying;
            break;
            }
        }
    }
    
    for (int n=0; n<2; n++)
    {
        for(int i=0;i<pieces[n].size();i++)
        {
            if(index==pieces[n][i]->position )
            {
                for (int n=0; n<2; n++)
                {
                    for(int i=0;i<pieces[n].size();i++)
                    {
                        if(pieces[n][i]->selected)
                        {
                            pieces[n][i]->selected=false;
                        }
                    }
                }
            }
        }
    }
    
    for (int n=0; n<2; n++)
    {
        for(int i=0;i<pieces[n].size();i++)
        {
            if(index==pieces[n][i]->position && isPlaying == pieces[n][i]->color )
                pieces[n][i]->selected=true;
            
        }
    }
  
    
    selected=index;
}

//--------------------------------------------------------------
void ofApp::mouseReleased(int x, int y, int button){

    
}

//--------------------------------------------------------------
void ofApp::mouseEntered(int x, int y){

}

//--------------------------------------------------------------
void ofApp::mouseExited(int x, int y){

}

//--------------------------------------------------------------
void ofApp::windowResized(int w, int h){

}

//--------------------------------------------------------------
void ofApp::gotMessage(ofMessage msg){

}

//--------------------------------------------------------------
void ofApp::dragEvent(ofDragInfo dragInfo){ 

}

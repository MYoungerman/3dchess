//
//  King.hpp
//  3DChess
//
//  Created by Marlon Youngerman on 25/12/2018.
//

#ifndef King_hpp
#define King_hpp

#include "Piece.hpp"

class King : public Piece{
    
public:
    King(ofVec3f p, bool c);
    bool legal(ofVec3f p);
    int numLegal();//returns number of legal moves
    bool isKing();
    
    
};

#endif /* King_hpp */

//
//  Queen.cpp
//  3DChess
//
//  Created by Marlon Youngerman on 25/12/2018.
//

#include "Queen.hpp"


Queen::Queen(ofVec3f p, bool c): Piece (p,c){
    
       model.loadModel("Queen.3ds");

}
/*
bool Queen::legal(ofVec3f p){
    
   if( (p.x!=position.x && p.y==position.y && p.z==position.z) || (p.x==position.x && p.y!=position.y && p.z==position.z) || (p.x==position.x && p.y==position.y && p.z!=position.z)  || (abs(p.y-position.y) ==  abs(p.x-position.x)  && p.z==position.z) || (abs(p.z-position.z) ==  abs(p.x-position.x)  && p.y==position.y) || (abs(p.z-position.z) ==  abs(p.y-position.y)  && p.x==position.x) )
   {
       return true;
   }
    else
    {
        return false;
    }
}
 */

bool Queen::legal(ofVec3f p){
    
    if(p==position)
        return false;
    
    if( (p.x!=position.x && p.y==position.y && p.z==position.z) )
    {
        for(int i=0;i<o.size();i++)
        {
            if( (o[i]->position.x!=position.x && o[i]->position.y==position.y && o[i]->position.z==position.z) )
            {
                if( (position.distance(o[i]->position) < position.distance(p)) && ( (o[i]->position.x>position.x && p.x>position.x)  or (o[i]->position.x<position.x && p.x<position.x) ) )
                return false;
            }
        }
        return true;
    }
    else if( (p.x==position.x && p.y!=position.y && p.z==position.z) )
    {
        for(int i=0;i<o.size();i++)
        {
            if( (o[i]->position.x==position.x && o[i]->position.y!=position.y && o[i]->position.z==position.z) )
            {
                if( position.distance(o[i]->position) < position.distance(p) && ( (o[i]->position.y>position.y && p.y>position.y)  or (o[i]->position.y<position.y && p.y<position.y) )  )
                    return false;
            }
        }
        return true;
    }

    else if( (p.x==position.x && p.y==position.y && p.z!=position.z))
    {
        for(int i=0;i<o.size();i++)
        {
            if( o[i]->position.x==position.x && o[i]->position.y==position.y )
                {
                    if( (position.distance(o[i]->position) < position.distance(p)) && ( (o[i]->position.z!=position.z  &&  o[i]->position.z>position.z && p.x>position.z)  or (o[i]->position.z<position.z && p.z<position.z) ) )
                        return false;
                }
        }
        return true;
    }
    else if( (abs(p.y-position.y) ==  abs(p.x-position.x)  && p.z==position.z))
    {
        for(int i=0;i<o.size();i++)
        {
            if( ( abs(o[i]->position.y-position.y) ==  abs(o[i]->position.x-position.x) )
                && ( o[i]->position.z==position.z && o[i]->position != position ) )
            {
                if( (position.distance(o[i]->position) < position.distance(p)) && ( abs(p.x-o[i]->position.x) ==  abs(p.y-o[i]->position.y) ) )
                    
                    return false;
            }
        }
        return true;
    }
    else if( (abs(p.z-position.z) ==  abs(p.x-position.x)  && p.y==position.y) )
    {
        for(int i=0;i<o.size();i++)
        {
            if( (abs(o[i]->position.z-position.z) ==  abs(o[i]->position.x-position.x)  && o[i]->position.y==position.y) && o[i]->position != position)
            {
                if( (position.distance(o[i]->position) < position.distance(p)) && ( abs(p.z-o[i]->position.z) ==  abs(p.x-o[i]->position.x) ) )
                {
                    return false;
                }
            }
        }
        return true;
    }
    else if( (abs(p.z-position.z) ==  abs(p.y-position.y)  && p.x==position.x)  )
    {
        for(int i=0;i<o.size();i++)
        {
            if((abs(o[i]->position.z-position.z) ==  abs(o[i]->position.y-position.y)  && o[i]->position.x==position.x) && o[i]->position != position )
            {
                if( (position.distance(o[i]->position) < position.distance(p)) && ( abs(p.z-o[i]->position.z) ==  abs(p.y-o[i]->position.y) ) )
                    return false;
            }
        }
        return true;
    }

    else
        {
            return false;
        }
}

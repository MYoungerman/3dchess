#pragma once

#include "ofMain.h"
#include "ofxAssimpModelLoader.h"
#include "Board.hpp"
#include "Piece.hpp"
#include "Rook.hpp"
#include "Bishop.hpp"
#include "King.hpp"
#include "Queen.hpp"
#include "Knight.hpp"
#include "Pawn.hpp"
#include <iostream>


class ofApp : public ofBaseApp{

	public:
		void setup();
		void update();
		void draw();

		void keyPressed(int key);
		void keyReleased(int key);
		void mouseMoved(int x, int y );
		void mouseDragged(int x, int y, int button);
		void mousePressed(int x, int y, int button);
		void mouseReleased(int x, int y, int button);
		void mouseEntered(int x, int y);
		void mouseExited(int x, int y);
		void windowResized(int w, int h);
		void dragEvent(ofDragInfo dragInfo);
		void gotMessage(ofMessage msg);
    
    
        Board board;

    
        ofVec3f selected;
    
        bool win;
        bool winColor;
        bool isPlaying;
        vector<Piece*> pieces[2];
        std::vector <ofVec3f> positions;//position of each piece
    
//        std::vector <Piece*> black;
//        std::vector <Piece*> white;
    
        ofEasyCam cam;
		
};

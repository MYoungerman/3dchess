//
//  Pawn.hpp
//  3DChess
//
//  Created by Marlon Youngerman on 25/12/2018.
//

#ifndef Pawn_hpp
#define Pawn_hpp

#include "Piece.hpp"


class Pawn : public Piece{
    
public:
    Pawn(ofVec3f p, bool c);
    bool legal(ofVec3f p);
};


#endif /* Pawn_hpp */
